﻿using GestionStockCore.DAL.Entities;
using System.ComponentModel.DataAnnotations;

namespace GestionStockCore.ASP.Models.Customers
{
    public class CustomerModel
    {
        public CustomerModel(Customer c)
        {
            Reference = c.Reference;
            LastName = c.LastName;
            FirstName = c.FirstName;
            Email = c.Email;
        }

        [Display(Name = "Ref.")]
        public string Reference { get; set; }

        [Display(Name = "Nom")]
        public string LastName { get; set; }

        [Display(Name = "Prénom")]
        public string FirstName { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
