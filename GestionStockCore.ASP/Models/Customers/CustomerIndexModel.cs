﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace GestionStockCore.ASP.Models.Customers
{
    public class CustomerIndexModel
    {
        public CustomerIndexModel()
        {
            Limit = 5;
            Offset = 0;
        }

        [Range(1, 100)]
        public int Limit { get; set; }

        [Range(0, int.MaxValue)]
        public int Offset { get; set; }
        public int Total { get; set; }
        public List<CustomerModel> Results { get; set; }
    }
}
