﻿using GestionStockCore.ASP.Models.Customers;
using GestionStockCore.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace GestionStockCore.ASP.Controllers
{
    public class CustomerController : Controller
    {
        private readonly StockContext _dc;

        public CustomerController(StockContext dc)
        {
            _dc = dc;
        }

        public IActionResult Index(CustomerIndexModel model)
        {
            if(ModelState.IsValid)
            {
                model.Results = _dc.Customers
                    .Skip(model.Offset)
                    .Take(model.Limit)
                    .Select(c => new CustomerModel(c))
                    .ToList();
                model.Total = _dc.Customers.Count();
                return View(model);
            }
            return ValidationProblem(ModelState);
        }
    }
}
