﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace GestionStockCore.ASP.TagHelpers
{
    public class PaginationTagHelper : TagHelper
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int Total { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (Limit >= Total) 
                return;

            output.TagName = "ul";
            output.Attributes.SetAttribute("class", "pagination");
            output.Content.AppendHtml(CreateLink("Précédent", Offset - Limit, Offset - Limit < 0));
            for (int i = 0; i < (double)Total / Limit; i++)
            {
                output.Content.AppendHtml(CreateLink(i + 1, i * Limit, false, i * Limit == Offset));
            }
            output.Content.AppendHtml(CreateLink("Suivant", Offset + Limit, Offset + Limit >= Total));
        }

        private static TagBuilder CreateLink(object title, int offset, bool disabled = false, bool active = false)
        {
            TagBuilder li = new("li");
            li.AddCssClass("page-item");
            if(disabled)
            {
                li.AddCssClass("disabled");
            }
            if (active)
            {
                li.AddCssClass("active");
            }
            TagBuilder a = new("a");
            a.AddCssClass("page-link");
            a.Attributes["href"] = disabled || active ? "#" : "?offset=" + offset;
            a.InnerHtml.Append(title.ToString());
            li.InnerHtml.AppendHtml(a);
            return li;
        }
    }
}
